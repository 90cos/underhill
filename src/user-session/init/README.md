# init

The init container generates a WireGuard configuration file that is compatible with Cloudflare's WARP service.

## Build instructions

```console
# Build the source APKs with melange (you must have the melange.rsa private key, or generate your own)
melange build melange.yaml --signing-key melange.rsa

# Create and publish the container from the APKs
apko publish apko.yaml registry.gitlab.com/90cos/underhill/init:<tag> -k melange.rsa.pub
```
