package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	// Register the container with WireGuard Cloudflare, accepting the terms of service
	if err := exec.Command("wgcf", "register", "--accept-tos").Run(); err != nil {
		fmt.Println("Error registering:", err)
		return
	}

	// Generate the WireGuard configuration file
	if err := exec.Command("wgcf", "generate").Run(); err != nil {
		fmt.Println("Error generating configuration:", err)
		return
	}

	// Create a directory for configurations
	if err := os.Mkdir("/tmp/wg_confs", 0755); err != nil {
		fmt.Println("Error creating directory:", err)
		return
	}

	// Read the generated profile
	data, err := os.ReadFile("wgcf-profile.conf")
	if err != nil {
		fmt.Println("Error reading profile:", err)
		return
	}

	// Remove IPv6 and DNS settings
	modified := removeUnwantedConfig(string(data))

	// Define additional configuration lines to maintain local access to the container
	linesToAdd := `PostUp = DROUTE=$(ip route | grep default | awk '{print $3}'); HOMENET=192.168.0.0/16; HOMENET2=10.0.0.0/8; HOMENET3=172.16.0.0/12; ip route add $HOMENET3 via $DROUTE;ip route add $HOMENET2 via $DROUTE; ip route add $HOMENET via $DROUTE;iptables -I OUTPUT -d $HOMENET -j ACCEPT;iptables -A OUTPUT -d $HOMENET2 -j ACCEPT; iptables -A OUTPUT -d $HOMENET3 -j ACCEPT;  iptables -A OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT
PreDown = HOMENET=192.168.0.0/16; HOMENET2=10.0.0.0/8; HOMENET3=172.16.0.0/12; ip route del $HOMENET3 via $DROUTE;ip route del $HOMENET2 via $DROUTE; ip route del $HOMENET via $DROUTE; iptables -D OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT; iptables -D OUTPUT -d $HOMENET -j ACCEPT; iptables -D OUTPUT -d $HOMENET2 -j ACCEPT; iptables -D OUTPUT -d $HOMENET3 -j ACCEPT`

	// Insert the additional configuration lines under the [Interface] section
	finalConfig := insertAdditionalConfig(modified, linesToAdd)

	// Save the modified configuration to /tmp/wg_confs/wg0.conf
	if err := os.WriteFile("/tmp/wg_confs/wg0.conf", []byte(finalConfig), 0644); err != nil {
		fmt.Println("Error writing final configuration:", err)
		return
	}

	// Display the final WireGuard configuration
	fmt.Println("WireGuard configuration file generated")
}

func removeUnwantedConfig(data string) string {
	var result strings.Builder
	lines := strings.Split(data, "\n")
	insideInterface := false
	for _, line := range lines {
		if strings.HasPrefix(line, "[Interface]") {
			insideInterface = true
		}
		if strings.HasPrefix(line, "[Peer]") {
			insideInterface = false
		}
		if insideInterface && (strings.Contains(line, "Address =") && strings.Contains(line, ":") || strings.Contains(line, "DNS =")) {
			continue
		}
		if !insideInterface && strings.Contains(line, "AllowedIPs = ::/0") {
			continue
		}
		result.WriteString(line + "\n")
	}
	return result.String()
}

func insertAdditionalConfig(data, linesToAdd string) string {
	return strings.Replace(data, "[Interface]", "[Interface]\n"+linesToAdd, 1)
}
