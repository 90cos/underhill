import express from "express";
import k8s from "@kubernetes/client-node";

const app = express();
const port = 3000;

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const k8sAppApi = kc.makeApiClient(k8s.AppsV1Api);
const k8sCoreApi = kc.makeApiClient(k8s.CoreV1Api);

// Load env vars or use defaults
const maxUserSessions = process.env.MAX_USER_SESSIONS
  ? parseInt(process.env.MAX_USER_SESSIONS, 10)
  : 1;
const sessionTimeout = process.env.SESSION_MAX_AGE_MINS
  ? parseInt(process.env.SESSION_MAX_AGE_MINS, 10)
  : 60;
const nodeEnv = process.env.NODE_ENV || " cannot find NODE_ENV";

// default k8s resource names
const fallbackNamespace = "underhill";
// const fallbackStatefulSetName = "user-session"

// Serve static files from the public directory
app.use(express.static("public"));

app.get("/listPods", async (req, res) => {
  const targetNamespace = (req.query.namespace || fallbackNamespace) as string;
  try {
    const podRes = await k8sCoreApi.listNamespacedPod(targetNamespace);
    const statusCode = podRes.response?.statusCode ?? 500;

    if (statusCode == 200) {
      const podStatuses = podRes.body.items.map((pod: any) => {
        return {
          name: pod.metadata?.name ?? "unknown",
          status: pod.status?.phase ?? "unknown",
          startTime: pod.status?.startTime ?? "unknown",
        };
      });

      res
        .status(200)
        .json({ pods: podStatuses, message: "Successfully listed pods" });
      return;
    } else {
      res.status(statusCode).json({ message: "Error with k8s Api" });
      return;
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to list pods" });
    return;
  }
});

app.get("/maxUserSessions", (_req, res) => {
  res.status(200).json({ maxUserSessions });
});

app.get("/sessionTimeout", (_req, res) => {
  res.status(200).json({ sessionTimeout });
});

app.get("/nodeEnv", (_req, res) => {
  res.status(200).json({ nodeEnv });
});

app.post("/startNextSession", async (req, res) => {
  const targetNamespace = (req.query.namespace || fallbackNamespace) as string;

  try {
    const { sessionId, message } = await getSessionIdToStart(targetNamespace);
    console.log(`SessionId: ${sessionId}\nMessage: ${message}`);
    if (sessionId === null) {
      res.status(409).json({ message: message });
      return;
    }
    if (!startSession(sessionId)) {
      res.status(400).json({ message: "Session already active" });
      return;
    }

    res.status(200).json({ sessionId: sessionId, message: "Session started" });
    return;
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error starting session" });
    return;
  }
});

app.get("/getLowestActiveSession", async (req, res) => {
  const targetNamespace = (req.query.namespace || fallbackNamespace) as string;

  try {
    const { sessionId, message } = await getSessionIdToStart(targetNamespace);
    if (sessionId === null) {
      res.status(409).json({ message: message });
      return;
    }

    res.status(200).json({ sessionId: sessionId, message: "Session started" });
    return;
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error getting lowest active session" });
    return;
  }
});

app.get("/podHealth/:id", async (req, res) => {
  const targetNamespace = (req.query.namespace || fallbackNamespace) as string;
  const id = req.params.id;

  if (id === undefined || isNaN(Number(id))) {
    res.status(400).json({
      message: "Invalid ID. ID must be a number.",
      status: "Error",
    });
    return;
  }

  try {
    const pods = await k8sCoreApi.listNamespacedPod(
      targetNamespace,
      undefined,
      undefined,
      undefined,
      undefined,
      `session-id=${id}`,
    );

    if (!pods.body || !pods.body.items || pods.body.items.length === 0) {
      res.status(404).json({
        message: "No pods found for the session",
        status: "NotFound",
      });
      return;
    }

    // Filter out pods that are in the process of being terminated
    const activePods = pods.body.items.filter(
      (pod: any) => !pod.metadata?.deletionTimestamp,
    );

    if (activePods.length === 0) {
      res.status(404).json({
        message: `No active pod found with id ${id}`,
        status: "Error",
      });
      return;
    } else if (activePods.length > 1) {
      res.status(500).json({
        message: `More than one pod found with id ${id}`,
        status: "Error",
      });
      return;
    } else if (!activePods[0].status) {
      res.status(500).json({
        message: `Pod status undefined`,
        status: "Error",
      });
      return;
    }

    const podStatus = isPodHealthy(activePods[0].status);

    if (podStatus.isReady) {
      res.status(200).json({ message: podStatus.message, status: "Healthy" });
      return;
    } else {
      res.status(503).json({ message: podStatus.message, status: "Unhealthy" });
      return;
    }
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .json({ message: "Failed to retrieve pod status", status: "Error" });
    return;
  }
});

app.post("/startSession/:id", async (req, res) => {
  const targetNamespace = (req.query.namespace || fallbackNamespace) as string;

  const id = req.params.id;
  if (id === undefined || isNaN(Number(id))) {
    res.status(400).json({
      message: "Invalid ID. ID must be a number.",
      status: "Error",
    });
    return;
  }

  try {
    const k8sRes = await k8sAppApi.readNamespacedDeployment(
      "user-session-" + id,
      targetNamespace,
    );
    let deployment = k8sRes.body;

    if (deployment.spec === undefined) {
      res.status(500).json({ message: "Deployment spec not found" });
      return;
    }

    if (deployment.spec.replicas === undefined) {
      deployment.spec.replicas = 0;
    }

    if (deployment.spec.replicas > 0) {
      res.status(400).json({ message: "Session already active" });
      return;
    } else {
      deployment.spec.replicas += 1;
    }

    await k8sAppApi.replaceNamespacedDeployment(
      "user-session-" + req.params.id,
      fallbackNamespace,
      deployment,
    );
    res.status(200).json({
      message: "Session started",
      sessionId: "user-session-" + req.params.id,
    });
    return;
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error starting session" });
    return;
  }
});

// TODO - throw 404 when deployment does not exist
app.delete("/endSession/:id", async (req, res) => {
  const targetNamespace = (req.query.namespace || fallbackNamespace) as string;

  const id = req.params.id;
  if (id === undefined || isNaN(Number(id))) {
    res.status(400).json({
      message: "Invalid ID. ID must be a number.",
      status: "Error",
    });
    return;
  }
  console.log(`End session request received for pod ${id}`);

  try {
    const k8sRes = await k8sAppApi.readNamespacedDeployment(
      "user-session-" + req.params.id,
      targetNamespace,
    );

    let deployment = k8sRes.body;

    if (deployment.spec === undefined) {
      res.status(500).json({ message: "Deployment spec not found" });
      return;
    }

    deployment.spec.replicas = 0;

    await k8sAppApi.replaceNamespacedDeployment(
      "user-session-" + req.params.id,
      fallbackNamespace,
      deployment,
    );
    res.status(200).json({
      message: "Session ended",
      sessionId: "user-session-" + req.params.id,
    });
    return;
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: `Error ending session ${id}` });
    return;
  }
});

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

/*
Helper functions
*/

function isPodHealthy(podStatus: any) {
  // Check the pod phase
  if (podStatus.phase !== "Running") {
    return {
      isReady: false,
      message: "Pod is not running.",
    };
  }

  // Check conditions
  const requiredConditions = [
    "Initialized",
    "Ready",
    "ContainersReady",
    "PodScheduled",
  ];
  for (let condition of podStatus?.conditions ?? []) {
    if (
      requiredConditions.includes(condition.type) &&
      condition.status !== "True"
    ) {
      return {
        isReady: false,
        message: `${condition.type} condition is not True.`,
      };
    }
  }

  // Check container statuses
  for (let container of podStatus?.containerStatuses ?? []) {
    if (!container.ready) {
      return {
        isReady: false,
        message: `${container.name} container is not ready.`,
      };
    }
  }

  // Check init container statuses
  for (let initContainer of podStatus?.initContainerStatuses ?? []) {
    if (
      !initContainer?.state?.terminated ||
      initContainer.state.terminated.reason !== "Completed"
    ) {
      return {
        isReady: false,
        message: `${initContainer.name} init container has not completed.`,
      };
    }
  }

  // If all checks passed, the pod is healthy
  return {
    isReady: true,
    message: "Pod is healthy and ready.",
  };
}

async function getSessionIdToStart(targetNamespace: string) {
  try {
    const deploymentRes = await k8sAppApi.listNamespacedDeployment(
      targetNamespace,
      undefined,
      undefined,
      undefined,
      undefined,
      "app=user-session",
    );
    if (deploymentRes.response.statusCode != 200) {
      throw new Error("Error with k8s Api");
    }

    // Filter out deployments with replicas set to 0 (i.e., inactive sessions)
    const inactiveSessions = deploymentRes.body.items.filter(
      (deployment: any) => deployment?.spec?.replicas === 0,
    );

    // If no inactive sessions found, handle accordingly
    if (inactiveSessions.length === 0) {
      return { sessionId: null, message: "No inactive sessions found" };
    }

    // Find deployment with the lowest session-id label using a loop
    let lowestInactiveSession = Number.MAX_SAFE_INTEGER;

    for (let deployment of inactiveSessions) {
      const sessionIdFromLabel = deployment?.metadata?.labels?.["session-id"];

      if (!sessionIdFromLabel) {
        console.error(
          `Failed to find session id from label for deployment ${deployment.metadata?.name}`,
        );
        continue;
      }

      const currentSessionId = parseInt(sessionIdFromLabel, 10);
      if (currentSessionId < lowestInactiveSession) {
        lowestInactiveSession = currentSessionId;
      }
    }

    return {
      sessionId: lowestInactiveSession,
      message: "Successfully retrieved the lowest inactive session id",
    };
  } catch (error) {
    console.error(error);
    throw new Error("Failed to retrieve the lowest inactive session id");
  }
}

async function startSession(sessionId: number) {
  const k8sRes = await k8sAppApi.readNamespacedDeployment(
    "user-session-" + sessionId,
    fallbackNamespace,
  );
  let deployment = k8sRes.body;

  if (deployment.spec?.replicas === undefined) {
    console.error(
      `Deployment spec replica undefined for user session ${sessionId} deployment`,
    );
    return false;
  }

  if (deployment.spec.replicas > 0) {
    return false;
  } else {
    deployment.spec.replicas += 1;
  }

  await k8sAppApi.replaceNamespacedDeployment(
    "user-session-" + sessionId,
    fallbackNamespace,
    deployment,
  );
}
