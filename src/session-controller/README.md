# session-controller

The session-controller provides an API server for provisioning and deprovisioning Underhill sessions.

## Build instructions

```console
# Build the source APKs with melange (you must have the melange.rsa private key, or generate your own with instructions below)
melange build melange.yaml --signing-key melange.rsa

# Create and publish the container from the APKs
apko publish apko.yaml registry.gitlab.com/90cos/underhill/session-controller:<tag> -k melange.rsa.pub
```

Generate temporary keypair with melange
```console
docker run --rm -v "${PWD}":/work cgr.dev/chainguard/melange keygen
```
