// Handler for starting next available session
const startNextSessionHandler = async () => {
  let popup = window.open("loading.html", "_blank"); // Open popup immediately
  try {
    const res = await fetch("/startNextSession", {
      method: "POST",
    });
    const data = await res.json();
    if (res.ok) {
      popup.location.href = `loading.html?sessionId=${data.sessionId}`;
    } else {
      popup.close(); // Close the popup if there was an error
      alert(data.message);
    }
  } catch (err) {
    popup.close(); // Close the popup in case of error
    alert(`Error: ${err.message}`);
    console.error(err);
  }
};

document
  .getElementById("startNextSession")
  .addEventListener("click", startNextSessionHandler);

// Function to fetch and display max user sessions and session timeout
const fetchAndDisplaySessionInfo = async () => {
  try {
    const maxUserSessionsRes = await fetch("/maxUserSessions");
    const maxUserSessionsData = await maxUserSessionsRes.json();
    document.getElementById("maxUserSessions").textContent =
      `Max User Sessions: ${maxUserSessionsData.maxUserSessions}`;

    const sessionTimeoutRes = await fetch("/sessionTimeout");
    const sessionTimeoutData = await sessionTimeoutRes.json();
    document.getElementById("sessionTimeout").textContent =
      `User Session Timeout: ${sessionTimeoutData.sessionTimeout} minutes`;
  } catch (error) {
    console.error("Error fetching session info:", error);
  }
};

// Initialize the Page
const initializePage = async () => {
  await fetchAndDisplaySessionInfo();
  fetchPods();
};

// Calculate alive time for each pod
function calculateAliveTime(startTime) {
  if (!startTime) return "unknown";

  const start = new Date(startTime);
  const now = new Date();
  const diffMs = now - start;

  const diffDays = Math.floor(diffMs / (1000 * 60 * 60 * 24));
  const diffHours = Math.floor(
    (diffMs % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
  );
  const diffMinutes = Math.floor((diffMs % (1000 * 60 * 60)) / (1000 * 60));

  let aliveTime = "";
  if (diffDays > 0) aliveTime += `${diffDays} days `;
  if (diffHours > 0) aliveTime += `${diffHours} hours `;
  if (diffMinutes > 0) aliveTime += `${diffMinutes} minutes`;

  return aliveTime.trim();
}

// Fetch pods and update the table
function fetchPods() {
  const podsTableBody = document.querySelector("#podStatusTable tbody");
  fetch("/listPods")
    .then((response) => response.json())
    .then((data) => {
      if (data.pods) {
        const pods = data.pods;
        podsTableBody.innerHTML = "";
        pods.forEach((pod) => {
          const row = document.createElement("tr");
          const nameCell = document.createElement("td");
          const statusCell = document.createElement("td");
          const actionCell = document.createElement("td");
          const actionCellDiv = document.createElement("div");
          actionCellDiv.style = "display: flex;";
          actionCell.append(actionCellDiv);
          const aliveTimeCell = document.createElement("td");

          // Extract the meaningful part of the pod name
          const cleanPodName = pod.name.split("-").slice(0, -2).join("-");
          nameCell.textContent = cleanPodName;
          statusCell.textContent = pod.status;
          aliveTimeCell.textContent = calculateAliveTime(pod.startTime);

          // Check if the pod name contains 'user-session'
          if (pod.name.includes("user-session")) {
            // Extract the session id
            const sessionId = cleanPodName.split("-").pop();

            // Open button
            const openButton = document.createElement("button");
            openButton.textContent = "Open";
            openButton.classList = "btn-small btn-primary";
            openButton.onclick = function () {
              window.open(`/sessions/user-session-${sessionId}/`);
            };
            actionCellDiv.append(openButton);

            // Delete button
            const deleteButton = document.createElement("button");
            deleteButton.textContent = "Delete";
            deleteButton.classList = "btn-small btn-secondary";
            deleteButton.onclick = function () {
              callDeletePod(sessionId);
            };
            actionCellDiv.appendChild(deleteButton);
          } else {
            actionCell.textContent = "N/A";
          }

          row.appendChild(nameCell);
          row.appendChild(statusCell);
          row.appendChild(actionCell);
          row.appendChild(aliveTimeCell);
          podsTableBody.appendChild(row);
        });
      } else {
        alert(data.message);
      }
    })
    .catch((error) => {
      console.error("Error fetching pods:", error);
    });
}

// Listener for listing pods
document.getElementById("listPods").addEventListener("click", () => {
  showElement("podStatusContainer");
  fetchPods();
});

// Show list pod box
function showElement(id) {
  document.getElementById(id).style.display = "block";
}

// Make an HTTP DELETE request to the API
function callDeletePod(sessionId) {
  fetch(`/endSession/${sessionId}`, {
    method: "DELETE",
  })
    .then((response) => {
      if (response.ok) {
        alert("Session is scheduled for deletion");
        fetchPods(); // Fetch the updated list of pods after deletion
      } else {
        alert("Failed to delete session");
      }
    })
    .catch((error) => {
      alert("Error deleting session:", error);
    });
}

// Initialize the page when DOM is loaded
document.addEventListener("DOMContentLoaded", initializePage);
