# Set the maximum number of user sessions, defaulting to 1 if not set
{{- $maxUserSessions := (.Values.userSession.maxCount | int) | default 1 -}}

# The . scope gets overridden in a control structure.
{{- $root := . -}}

# Loop to create multiple deployments based on maxUserSessions
{{- range $i, $e := until $maxUserSessions }}

# Deployment for each user session
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "user-session-{{ $i }}"  # Dynamic session name
  namespace: {{ $root.Release.Namespace }}
  labels:
    app: user-session  # Label for app grouping
    session-id: "{{ $i }}" # Need this to idetify deployments by ID
spec:
  replicas: 0  # No replicas initially
  selector:
    matchLabels:
      app: user-session
      session-id: "{{ $i }}"  # Dynamic session ID
  template:
    metadata:
      {{- if or $root.Values.commonAnnotations $root.Values.userSession.additionalAnnotations }}
      annotations:
      {{- end }}
      {{- if $root.Values.commonAnnotations }}
        {{ toYaml $root.Values.commonAnnotations }}
      {{- end }}
      {{- if $root.Values.userSession.additionalAnnotations }}
        {{ toYaml $root.Values.userSession.additionalAnnotations }}
      {{- end }}
      labels:
        app: user-session
        session-id: "{{ $i }}"
        {{- if $root.Values.commonLabels }}
        {{ toYaml $root.Values.commonLabels }}
        {{- end }}
        {{- if $root.Values.userSession.additionalLabels }}
        {{ toYaml $root.Values.userSession.additionalLabels }}
        {{- end }}
    spec:
      containers:
        # Browser container
        - name: browser
          image: {{ $root.Values.userSession.browser.image.repository }}:{{ $root.Values.userSession.browser.image.tag }}
          imagePullPolicy: {{ $root.Values.userSession.browser.image.pullPolicy }}
          {{- if $root.Values.userSession.browser.securityContext }}
          securityContext:
            {{- toYaml $root.Values.userSession.browser.securityContext | nindent 12 }}
          {{- end }}
          ports:
            - containerPort: 3000
          env:
            - name: SESSION_ID
              value: "{{ $i }}"  # Dynamic session ID
            - name: SUBFOLDER
              value: "/sessions/user-session-{{ $i }}/"
          resources:
            requests:
              memory: {{ $root.Values.userSession.browser.resources.requests.memory }}
              cpu: {{ $root.Values.userSession.browser.resources.requests.cpu }}
            limits:
              memory: {{ $root.Values.userSession.browser.resources.limits.memory }}
              cpu: {{ $root.Values.userSession.browser.resources.limits.cpu }}

        - name: wireguard
          image: {{ $root.Values.userSession.wireguard.image.repository }}:{{ $root.Values.userSession.wireguard.image.tag }}
          imagePullPolicy: {{ $root.Values.userSession.wireguard.image.pullPolicy }}
          {{- if $root.Values.userSession.wireguard.securityContext }}
          securityContext:
            {{- toYaml $root.Values.userSession.wireguard.securityContext | nindent 12 }}
          {{- end }}
          resources:
            requests:
              memory: {{ $root.Values.userSession.wireguard.resources.requests.memory }}
              cpu: {{ $root.Values.userSession.wireguard.resources.requests.cpu }}
            limits:
              memory: {{ $root.Values.userSession.wireguard.resources.limits.memory }}
              cpu: {{ $root.Values.userSession.wireguard.resources.limits.cpu }}
          volumeMounts:
            - name: wireguard-config
              mountPath: /config/wg0.conf
              subPath: wg_confs/wg0.conf

      # Image pull secret for private registry
      imagePullSecrets:
        - name: underhill-private-registry

      # Init container for setup
      initContainers:
        - name: init
          image: {{ $root.Values.userSession.init.image.repository }}:{{ $root.Values.userSession.init.image.tag }}
          imagePullPolicy: {{ $root.Values.userSession.init.image.pullPolicy }}
          {{- if $root.Values.userSession.init.securityContext }}
          securityContext:
            {{- toYaml $root.Values.userSession.init.securityContext | nindent 12 }}
          {{- end }}
          resources:
            requests:
              memory: {{ $root.Values.userSession.init.resources.requests.memory }}
              cpu: {{ $root.Values.userSession.init.resources.requests.cpu }}
            limits:
              memory: {{ $root.Values.userSession.init.resources.limits.memory }}
              cpu: {{ $root.Values.userSession.init.resources.limits.cpu }}
          volumeMounts:
            - name: wireguard-config
              mountPath: /tmp  # Temporary path for init setup

      # Volume for wireguard config
      volumes:
        - name: wireguard-config
{{- end }}  # End of Deployment loop
