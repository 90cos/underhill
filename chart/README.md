# Chart Overview

The current helm chart consists of a user session app, session controller app, and a deployment manager.

## User session

The user session app consists of four containers:

  1. Init container - An Alpine container which generates a WireGuard configuration file using Cloudflare WARP via the [wgcf](https://github.com/ViRb3/wgcf) project.
  2. noVNC - An Alpine container which runs the [noVNC](https://novnc.com/info.html) project to serve the browser container's window session.
  3. browser - A Debian container which contains Firefox and the x11vnc server advertising on port 5900.
  4. wiregaurd - Local import of the [docker-wireguard](https://github.com/linuxserver/docker-wireguard/tree/v1.0.20210914-ls126) project by the [LinuxServer.io](https://linuxserver.io/) team on GitHub.
  5. monitor - An Alpine container which monitors the `browser` container and terminates the deployment if inactivity is detected.

## Session controller

Session controller is a single container deployment. It is an Express.JS Node server using [Kubernetes JS Client](https://github.com/kubernetes-client/javascript) to manage user sessions.

## Deployment manager

The deployment-manager creates a "underhill-service-account" service account in the "underhill" namespace.  It also creates a cluster-role called "deployment-manager" and binds that role to "underhill-service-account" service account.  

*note* The session controller needs this service account binding to control the user-session deployment
