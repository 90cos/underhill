# Underhill: Enhanced Internet Search Privacy

**Underhill** is an innovative solution designed to empower internet users with heightened privacy. By leveraging browser-in-browser sessions combined with VPN connections to Cloudflare, Underhill ensures a more secure and private browsing experience. 

## Table of Contents

- [Features](#features)
- [Local Development](#local-development)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Resetting the Cluster](#resetting-the-cluster)
- [License](#license)

## Features

- **Browser-in-Browser Sessions**: Isolate your browsing sessions for enhanced security and privacy.
- **VPN to Cloudflare**: Protect your searches by routing them through a secure VPN to Cloudflare.
- **Intuitive User Interface**: Easily start, monitor, and manage your secure sessions.

## Local Development

### Prerequisites

1. Access to a Kubernetes cluster with Istio pre-installed and configured with a public ingress gateway.
2. Helm installation.
3. Access rights to the Underhill GitLab project registry.

### Installation

1. **Clone the Repository**: If you haven't already, make sure to clone the Underhill repository.

    ```bash
    git clone git@gitlab.com:underhill/underhill.git
    ```

2. **Install the Helm Chart**: Deploy Underhill into the `underhill` namespace.

    ```bash
    helm install underhill chart/ --namespace=underhill  --create-namespace
    ```

3. **Port Forwarding**: Forward the ports for Big Bang's Istio gateway.

    ```bash
    kubectl port-forward {ISTIO-INGRESS-GATEWAY} -n istio-system 8080:80
    ```

    > **Tip**: With Big Bang, the ingress gateway defaults to `svc/public-ingressgateway`.

4. **Access the Session Controller**: Use your browser to go to `http://localhost:8080`.

### Resetting the Cluster

To completely reset your cluster and remove all configurations, execute:

```bash
helm uninstall underhill --namespace=underhill
```
> **Warning**: After uninstallation, pause for about 1 minute ensuring all resources have been fully terminated before you reinstall the Helm release.

## License

This project is licensed under the [GNU General Public License](/LICENSE).
